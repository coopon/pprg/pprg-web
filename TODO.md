% TODO

# User
- Return error if user already registered
- Return error on incorrect login

# Sync 
- Everything

# Video 
- Link to Peertube links

# Session 
- Handle cookies

# Backup 
- Automate backups
- Allow downloading of CSVs

