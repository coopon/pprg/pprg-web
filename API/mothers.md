# /center/:center-id/mothers/all 

## GET 

### Response 
- application/json

[{"door_no":"C4","deleted":false,"age":23,"delivery_date":"2019-12-05T18:30:00Z","current_visit":0,"street_name":"Residency Apts","name":"Jenny","city":"Communistan","type":"","child_weight":"2","child_gender":"Female","center":1,"pin_code":"111000","blood_group":"O+","id":1,"area":"Sithankudi","mobile":"9876543210","worker":"kamal@velan.com"}]
