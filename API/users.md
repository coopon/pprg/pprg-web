# /users

## GET
- application/json

- Returns tuple of dicts containing all users

### Response
[{"deleted":false,"email":"karmic@koalamail.com","deleted_at":null,"password":"bcrypt+blake2b-512$d84c04e2e4119413a108ab060ff3e2d5$12$88f2f0cc0806d8bc4580d11bae0c2fa6702c8572a2057046","name":"Karmic Koala","center":1,"id":1,"officer":true,"mobile":"9876543210","created_at":null}]

# /users/authenticate

Used for logging in a user 

## POST 
- application/json

### Request 
{email "karmic@koalamail.com", password "koalafun"}

### Response

{
    "deleted": false,
    "email": "karmic@koalamail.com",
    "deleted_at": null,
    "password": "bcrypt+blake2b-512$d84c04e2e4119413a108ab060ff3e2d5$12$88f2f0cc0806d8bc4580d11bae0c2fa6702c8572a2057046",
    "name": "Karmic Koala",
    "center": 1,
    "id": 1,
    "officer": true,
    "mobile": "9876543210",
    "center-name": "FSHM",
    "created_at": null
}


# /user/:user-id

## GET 
- application/json
- Single dict / key-map containing all details of user 
### Response

{"deleted":false,"email":"karmic@koalamail.com","deleted_at":null,"password":"bcrypt+blake2b-512$d84c04e2e4119413a108ab060ff3e2d5$12$88f2f0cc0806d8bc4580d11bae0c2fa6702c8572a2057046","name":"Karmic Koala","center":1,"id":1,"officer":true,"mobile":"9876543210","created_at":null}

# /user/:user-id/edit

## POST
- application/x-www-form-urlencoded
### Request
{
id:	1
name:	Kamalavelan+S
mobile:	9
center:	1,
officer:checked}

### Response 
- Redirected to homepage if successful
- HTTP 302 "/" 


# /user/:user-id/mothers/all

## GET 

### Response 
- application/json

