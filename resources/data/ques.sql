--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: questionnaire; Type: TABLE; Schema: public; Owner: pprg
--

CREATE TABLE public.questionnaire (
    id integer NOT NULL,
    days integer,
    question character varying(255),
    subject character varying(20),
    task character varying(30)
);


ALTER TABLE public.questionnaire OWNER TO pprg;

--
-- Name: questionnaire_id_seq; Type: SEQUENCE; Schema: public; Owner: pprg
--

CREATE SEQUENCE public.questionnaire_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questionnaire_id_seq OWNER TO pprg;

--
-- Name: questionnaire_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pprg
--

ALTER SEQUENCE public.questionnaire_id_seq OWNED BY public.questionnaire.id;


--
-- Name: questionnaire id; Type: DEFAULT; Schema: public; Owner: pprg
--

ALTER TABLE ONLY public.questionnaire ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_id_seq'::regclass);


--
-- Data for Name: questionnaire; Type: TABLE DATA; Schema: public; Owner: pprg
--

COPY public.questionnaire (id, days, question, subject, task) FROM stdin;
1082	3	When passed urine & meconium?	Baby	Ask
1083	7	When passed urine & meconium?	Baby	Ask
1084	22	When passed urine & meconium?	Baby	Ask
1085	3	Difficulties in breastfeeding?	Baby	Ask
1086	7	Difficulties in breastfeeding?	Baby	Ask
1087	22	Difficulties in breastfeeding?	Baby	Ask
1088	3	Fever?	Baby	Ask
1089	7	Fever?	Baby	Ask
1090	22	Fever?	Baby	Ask
1091	3	Not suckling well?	Baby	Ask
1092	7	Not suckling well?	Baby	Ask
1093	22	Not suckling well?	Baby	Ask
1094	3	Any complaints?	Baby	Ask
1095	7	Any complaints?	Baby	Ask
1096	22	Any complaints?	Baby	Ask
1097	3	Respiratory rate	Baby	Examine
1098	7	Respiratory rate	Baby	Examine
1099	22	Respiratory rate	Baby	Examine
1100	3	Baby's color - Pallor / Jaundice/ Cyanosis	Baby	Examine
1101	7	Baby's color - Pallor / Jaundice/ Cyanosis	Baby	Examine
1102	22	Baby's color - Pallor / Jaundice/ Cyanosis	Baby	Examine
1103	3	Baby's body temperature	Baby	Examine
1104	7	Baby's body temperature	Baby	Examine
1105	22	Baby's body temperature	Baby	Examine
1106	3	Exclusive breastfeeding	Baby	Counsel
1107	7	Exclusive breastfeeding	Baby	Counsel
1108	22	Exclusive breastfeeding	Baby	Counsel
1109	3	Hygiene of the baby	Baby	Counsel
1110	7	Hygiene of the baby	Baby	Counsel
1111	22	Hygiene of the baby	Baby	Counsel
1112	3	When & where to seek help in case of signs of illness	Baby	Counsel
1113	7	When & where to seek help in case of signs of illness	Baby	Counsel
1114	22	When & where to seek help in case of signs of illness	Baby	Counsel
1115	3	Immunization	Baby	Counsel
1116	7	Immunization	Baby	Counsel
1117	22	Immunization	Baby	Counsel
1118	3	Any bleeding?	Mother	Ask
1119	7	Any bleeding?	Mother	Ask
1120	22	Any bleeding?	Mother	Ask
1121	3	Any vaginal discharge?	Mother	Ask
1122	7	Any vaginal discharge?	Mother	Ask
1123	22	Any vaginal discharge?	Mother	Ask
1124	3	Fever?	Mother	Ask
1125	7	Fever?	Mother	Ask
1126	22	Fever?	Mother	Ask
1127	3	Breast swelling?	Mother	Ask
1128	7	Breast swelling?	Mother	Ask
1129	22	Breast swelling?	Mother	Ask
1130	3	Pain during passing urine	Mother	Ask
1131	7	Pain during passing urine	Mother	Ask
1132	22	Pain during passing urine	Mother	Ask
1133	3	Not feeling well?	Mother	Ask
1134	7	Not feeling well?	Mother	Ask
1135	22	Not feeling well?	Mother	Ask
1136	3	Unhappiness?	Mother	Ask
1137	7	Unhappiness?	Mother	Ask
1138	22	Unhappiness?	Mother	Ask
1139	3	BP, Pulse, Temperature	Mother	Examine
1140	7	BP, Pulse, Temperature	Mother	Examine
1141	22	BP, Pulse, Temperature	Mother	Examine
1142	3	Abdominal & vulval examination	Mother	Examine
1143	7	Abdominal & vulval examination	Mother	Examine
1144	22	Abdominal & vulval examination	Mother	Examine
1145	3	Check breasts and nipples	Mother	Examine
1146	7	Check breasts and nipples	Mother	Examine
1147	22	Check breasts and nipples	Mother	Examine
1148	3	Diet	Mother	Counsel
1149	7	Diet	Mother	Counsel
1150	22	Diet	Mother	Counsel
1151	3	Contraception	Mother	Counsel
1152	7	Contraception	Mother	Counsel
1153	22	Contraception	Mother	Counsel
1154	14	Received recommended vaccines so far?	Baby	Ask
1155	28	Received recommended vaccines so far?	Baby	Ask
1156	42	Received recommended vaccines so far?	Baby	Ask
1157	14	Does baby take breastfeeds well?	Baby	Ask
1158	28	Does baby take breastfeeds well?	Baby	Ask
1159	42	Does baby take breastfeeds well?	Baby	Ask
1160	14	Does baby have any problems?	Baby	Ask
1161	28	Does baby have any problems?	Baby	Ask
1162	42	Does baby have any problems?	Baby	Ask
1163	14	Weight of the baby	Baby	Examine
1164	28	Weight of the baby	Baby	Examine
1165	42	Weight of the baby	Baby	Examine
1166	14	Check if baby is active or lethargic	Baby	Examine
1167	28	Check if baby is active or lethargic	Baby	Examine
1168	42	Check if baby is active or lethargic	Baby	Examine
1169	14	Importance of exclusive breastfeeding	Baby	Counsel
1170	28	Importance of exclusive breastfeeding	Baby	Counsel
1171	42	Importance of exclusive breastfeeding	Baby	Counsel
1172	14	Immunization	Baby	Counsel
1173	28	Immunization	Baby	Counsel
1174	42	Immunization	Baby	Counsel
1175	14	About emergency care unit if baby is having problems	Baby	Counsel
1176	28	About emergency care unit if baby is having problems	Baby	Counsel
1177	42	About emergency care unit if baby is having problems	Baby	Counsel
1178	14	Has vaginal bleeding stopped?	Mother	Ask
1179	28	Has vaginal bleeding stopped?	Mother	Ask
1180	42	Has vaginal bleeding stopped?	Mother	Ask
1181	14	Has menstrual cycle resumed?	Mother	Ask
1182	28	Has menstrual cycle resumed?	Mother	Ask
1183	42	Has menstrual cycle resumed?	Mother	Ask
1184	14	Any vaginal discharge?	Mother	Ask
1185	28	Any vaginal discharge?	Mother	Ask
1186	42	Any vaginal discharge?	Mother	Ask
1187	14	Pain during passing urine?	Mother	Ask
1188	28	Pain during passing urine?	Mother	Ask
1189	42	Pain during passing urine?	Mother	Ask
1190	14	Does not feel well?	Mother	Ask
1191	28	Does not feel well?	Mother	Ask
1192	42	Does not feel well?	Mother	Ask
1193	14	Any problems with breastfeeding	Mother	Ask
1194	28	Any problems with breastfeeding	Mother	Ask
1195	42	Any problems with breastfeeding	Mother	Ask
1196	14	Blood pressure	Mother	Examine
1197	28	Blood pressure	Mother	Examine
1198	42	Blood pressure	Mother	Examine
1199	14	Check pallor	Mother	Examine
1200	28	Check pallor	Mother	Examine
1201	42	Check pallor	Mother	Examine
1202	14	Examine vulva & perinium	Mother	Examine
1203	28	Examine vulva & perinium	Mother	Examine
1204	42	Examine vulva & perinium	Mother	Examine
1205	14	Breast & nipples	Mother	Examine
1206	28	Breast & nipples	Mother	Examine
1207	42	Breast & nipples	Mother	Examine
1208	14	Diet & Rest	Mother	Counsel
1209	28	Diet & Rest	Mother	Counsel
1210	42	Diet & Rest	Mother	Counsel
1211	14	Nutrition	Mother	Counsel
1212	28	Nutrition	Mother	Counsel
1213	42	Nutrition	Mother	Counsel
1214	14	Contraception	Mother	Counsel
1215	28	Contraception	Mother	Counsel
1216	42	Contraception	Mother	Counsel
\.


--
-- Name: questionnaire_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pprg
--

SELECT pg_catalog.setval('public.questionnaire_id_seq', 1216, true);


--
-- Name: questionnaire questionnaire_pkey; Type: CONSTRAINT; Schema: public; Owner: pprg
--

ALTER TABLE ONLY public.questionnaire
    ADD CONSTRAINT questionnaire_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

