(ns pprg.service
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [io.pedestal.interceptor :as interceptor]
            [io.pedestal.interceptor.chain :as interceptor.chain]
            [io.pedestal.interceptor.error :refer [error-dispatch]]
            [ring.util.response :as ring-resp]
            [pprg.site.home :as home]
            [ring.middleware.session.cookie :as cookie]
            [buddy.auth :as auth]
            [buddy.auth.backends :as auth.backends]
            [buddy.auth.middleware :as auth.middleware]
            [cheshire.core :as json]
            [pprg.site.users :as site.users]
            [pprg.site.centers :as site.centers]
            [pprg.site.mothers :as site.mothers]
            [pprg.site.auth :as site.auth]
            [pprg.site.questions :as site.questions]
            [pprg.site.visits :as site.visits]
;;            [pprg.site.questionnaire :as site.questionnnaire]
            ;; [mount.core :refer defstate]
            ))

(def secret (:secret (clojure.edn/read-string (slurp "resources/env/secret.edn"))))

(defn unauthorized-handler
  "This is the default action for buddy-auth's unathorized exception."
  [request metadata]
  (ring-resp/redirect "/"))

(def backend (auth.backends/session {:unauthorized-handler unauthorized-handler}))

(defn authorized
  "Returns a 200 response for authorized users, otherwise throws a buddy-auth 'unauthorized' exception."
  [request]
  (if (auth/authenticated? request)
    (ring-resp/response  "Only users can see this!")
    (auth/throw-unauthorized)))

(defn about-page
  [request]
  (ring-resp/response (format "Clojure %s - served from %s"
                              (clojure-version)
                              (route/url-for ::about-page))))



;; https://github.com/pedestal/pedestal/blob/master/samples/buddy-auth/src/buddy_auth/service.clj


(def content-length-json-body
  (interceptor/interceptor
   {:name ::content-length-json-body
    :leave (fn [context]
             (let [response (:response context)
                   body (:body response)
                   json-response-body (if body (json/generate-string body) "")
                    ;; Content-Length is the size of the response in bytes
                    ;; Let's count the bytes instead of the string, in case there are unicode characters
                   content-length (count (.getBytes ^String json-response-body))
                   headers (:headers response {})]
               (assoc context
                      :response {:status (:status response)
                                 :body json-response-body
                                 :headers (merge headers
                                                 {"Content-Type" "application/json;charset=UTF-8"
                                                  "Content-Length" (str content-length)})})))}))

(def authentication-interceptor
  "Port of buddy-auth's wrap-authentication middleware."
  (interceptor/interceptor
   {:name ::authenticate
    :enter (fn [ctx]
             (update
              ctx
              :request auth.middleware/authentication-request backend))}))

(defn authorization-interceptor
  "Port of buddy-auth's wrap-authorization middleware."
  [backend]
  (error-dispatch [ctx ex]
                  [{:exception-type :clojure.lang.ExceptionInfo :stage :enter}]
                  (try
                    (assoc ctx
                           :response
                           (auth.middleware/authorization-error (:request ctx)
                                                                ex
                                                                backend))
                    (catch Exception e
                      (assoc ctx ::interceptor.chain/error e)))
                  :else (assoc ctx ::interceptor.chain/error ex)))


;; Defines "/" and "/about" routes with their associated :get handlers.
;; The interceptors defined after the verb map (e.g., {:get home-page}
;; apply to / and its children (/about).


(def common-interceptors [(body-params/body-params) http/html-body authentication-interceptor (authorization-interceptor backend)])
(def json-interceptors [(body-params/body-params) content-length-json-body])
(def route-interceptors (vector common-interceptors route/path-params-decoder))
(defn roles-interceptor [role-list]

  (def common-interceptors [(body-params/body-params) http/html-body authentication-interceptor (authorization-interceptor backend)]))
;; Tabular routes


(def routes #{["/" :get (conj common-interceptors `home/home-page)]
              ["/about" :get (conj common-interceptors `about-page)]
              ["/authorized" :get (conj common-interceptors `authorized)]
              ;; URLs to create the new base stuff
              ["/create/center" :get  (conj common-interceptors `site.centers/create-center-page) :route-name :center-create-get]
              ["/create/center" :post (conj common-interceptors `site.centers/create-center-post) :route-name :center-create-post]
              ["/create/user" :get (conj common-interceptors `site.users/create-user-page) :route-name :user-create-get]
              ["/create/user" :post (conj common-interceptors `site.users/create-user-post) :route-name :user-create-post]
              ["/create/visit" :get (conj common-interceptors `site.visits/visit-add-page) :route-name :visit-add-page]
              ["/create/visit" :post (conj common-interceptors `site.visits/visit-add-post) :route-name :visit-add-post]
;; ;;
              ["/users" :get (conj json-interceptors `site.users/user-all)]
              ["/users/login" :get (conj common-interceptors `site.auth/auth-page) :route-name :user-login-page]
              ["/users/login" :post (conj common-interceptors `site.users/login-post) :route-name :user-login-post]
              ["/users/authenticate" :post (conj json-interceptors `site.users/api-login-post) :route-name :user-api-login-post]

              ;;               ;; URLs for individual users
              ["/user/:user-id" :get (conj json-interceptors `site.users/user-record-api) :route-name :user-record-api]
              ["/user/:user-id/edit" :get (conj common-interceptors `site.users/user-edit-page) :route-name :edit-user-page]
              ["/user/:user-id/edit" :post (conj common-interceptors `site.users/user-edit-post) :route-name :edit-user-post]
              ["/user/:user-id/delete" :get (conj common-interceptors `site.users/user-delete-page) :route-name :delete-user-page]
              ["/user/:user-id/delete" :post (conj common-interceptors `site.users/user-delete-post) :route-name :delete-user-post]
              ["/user/:user-id/mothers/all" :get (conj json-interceptors `site.mothers/mothers-by-worker) :route-name :mothers-all-by-worker]

               ;; Center stuff
              ["/center/:center-id/mothers/create" :get (conj common-interceptors `site.mothers/create-mother-page) :route-name :mother-create-get]
              ["/center/:center-id/mothers/create" :post (conj common-interceptors `site.mothers/create-mother-post) :route-name :mother-create-post]
              ["/center/:center-id" :get (conj common-interceptors `site.centers/center-overview-page) :route-name :center-overview-page]
              ["/center/:center-id/edit" :get (conj common-interceptors `site.centers/edit-center-page) :route-name :center-edit-page]
              ["/center/:center-id/edit" :post (conj common-interceptors `site.centers/edit-center-post) :route-name :center-edit-post]
              ["/center/:center-id/users" :get (conj common-interceptors `site.centers/users-all-page) :route-name :center-users-all-page]
              ["/center/:center-id/mothers" :get (conj common-interceptors `site.centers/mothers-all-page) :route-name :center-mothers-all-page]
              ["/center/:center-id/mothers/all" :get (conj json-interceptors `site.mothers/mothers-all) :route-name :mothers-all-by-center]
              ["/center/:center-id/mothers/sync" :post (conj json-interceptors `site.mothers/mothers-sync) :route-name :mothers-sync-by-center]
              ["/center/:center-id/delete" :get (conj common-interceptors `site.centers/center-delete-page) :route-name :center-delete-page]
              ["/center/:center-id/delete" :post (conj common-interceptors `site.centers/center-delete-post) :route-name :center-delete-post]

              ["/mother/:mother-id" :get (conj common-interceptors `site.mothers/mother-record-page) :route-name :mother-record-page]
              ["/mother/:mother-id/update" :post (conj common-interceptors `site.mothers/mother-record-update) :route-name :mother-record-update-post]
              ["/mother/:mother-id/edit" :get (conj common-interceptors `site.mothers/mother-record-page) :route-name :mother-record-edit]
              ["/mother/:mother-id/delete" :get (conj common-interceptors `site.mothers/mother-delete-page) :route-name :mother-delete-page]
              ["/mother/:mother-id/delete" :post (conj common-interceptors `site.mothers/mother-delete-post) :route-name :mother-delete-post]

              ;;               ;; Other stuff
              ["/visits/:visit-id" :get (conj common-interceptors `site.visits/visit-page) :route-name :visit-page]
;;
              })

;; Map-based routes
;(def routes `{"/" {:interceptors [(body-params/body-params) http/html-body]
;                   :get home-page
;                   "/about" {:get about-page}}})

;; Terse/Vector-based routes
;(def routes
;  `[[["/" {:get home-page}
;      ^:interceptors [(body-params/body-params) http/html-body]
;      ["/about" {:get about-page}]]]])


;; Consumed by pprg.server/create-server
;; See http/default-interceptors for additional options you can configure


(def service {:env :prod
              ;; You can bring your own non-default interceptors. Make
              ;; sure you include routing and set it up right for
              ;; dev-mode. If you do, many other keys for configuring
              ;; default interceptors will be ignored.
              ;; ::http/interceptors []
              ::http/routes routes

              ;; Uncomment next line to enable CORS support, add
              ;; string(s) specifying scheme, host and port for
              ;; allowed source(s):
              ;;
              ;; "http://localhost:8080"
              ;;
              ;;::http/allowed-origins ["scheme://host:port"]

              ;; Tune the Secure Headers
              ;; and specifically the Content Security Policy appropriate to your service/application
              ;; For more information, see: https://content-security-policy.com/
              ;;   See also: https://github.com/pedestal/pedestal/issues/499
              ::http/secure-headers {:content-security-policy-settings {:default-src "'self' 'unsafe-inline'"
                                                                        :img-src "'self' *.tile.openstreetmap.org "
                                                                        :script-src "'self' 'unsafe-inline'"}}
              ;;                                                          :frame-ancestors "'none'"}}

              ;; Root for resource interceptor that is available by default.
              ::http/resource-path "/public"

              ;; Either :jetty, :immutant or :tomcat (see comments in project.clj)
              ;;  This can also be your own chain provider/server-fn -- http://pedestal.io/reference/architecture-overview#_chain_provider
              ::http/enable-session {:store (cookie/cookie-store {:key secret})}
              ::http/type :immutant
              ;;::http/host "localhost"
              ::http/port 27273
              ;; Options to pass to the container (Jetty)
              ::http/container-options {:h2c? true
                                        :h2? false
                                        ;:keystore "test/hp/keystore.jks"
                                        ;:key-password "password"
                                        ;:ssl-port 8443
                                        :ssl? false}})

