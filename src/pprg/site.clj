(ns pprg.site
  (:require  [ring.util.response :as ring-resp]
             [pprg.db :as db]
             [pprg.db.centers :as db.centers])
  (:use
   [hiccup.page :only (html5 include-css include-js)]))

(defn base [title & content]
  (html5 {:lang "en"}
         [:head
          [:meta {:charset "utf-8"}]
          [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
          [:link {:rel "icon" :type "image/png" :href "/img/favicon.png"}]
          [:title title]
          ;;          (include-css "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css")]
          (include-css "/css/bulma.min.css")
          [:link {:rel "stylesheet" :href "/css/fork-awesome.min.css"}]] [:body {:style "background-color:fafafa;padding-right:40px;"} content]))

(defn form-control
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "text" :placeholder text :name param-name :value  value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-required
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "text" :placeholder text :name param-name :value  value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-number
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "number" :step "0.1" :placeholder text :name param-name :value  value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-textarea
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:textarea.textarea {:type "text" :placeholder text :name param-name :value value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-email
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "email" :placeholder text :name param-name :value value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-date
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control
                                   [:input.input {:type "date" :name param-name :value value}]]
   [:p.help "Click to choose a date"]])

(defn form-control-password
  "Password form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "password" :placeholder text :name param-name :value  value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-checkbox
  "Checkbox form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.checkbox text] [:div.control [:input {:type "checkbox" :placeholder text :name param-name :value value}]]])

(defn form-hidden-value
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.control [:input {:type "hidden" :name param-name :value value}]])

(defn form-control-dropdown
  "Dropdown form control for dynamic deployment. Accepts label name, form value name and value pairs for each option."
  [text name option-pairs]

  [:div.field [:label.label text]
   [:div.control
    [:div.select.is-primary
     [:select {:name name}
      (for [center option-pairs]
        [:option {:value (second center)} (first center)])]]]])

;;      (for [x options y values]
;;       [:option {:value (str y)} (str x)])]]]])


(defn form-control-button [name]
  [:div.control [:button.button.is-primary.is-medium name]])

(defn dash-counter [title]
  [:div.column.is-3
   [:div.card [:header.card-header [:p.card-header-title "Centers"] [:a.card-header-icon {:href "#" :aria-label "more options"} [:span.icon [:i.fas.fa-angle-down {:aria-hidden "true"}]]]] [:div.card-content [:div.content "Something here"  [:br]]] [:footer.card-footer [:a.card-footer-item {:href "#"} "Add new"]]]])

(defn admin-template [title & page]
  (base title
        [:div {:style "padding-top:1em;"}
         [:div.columns
          [:div.column.is-2 {:style "padding-left:2em;border-right:solid hsl(0, 0%, 96%, 1);"}
           [:aside.menu
            [:p.menu-label "General"]
            [:ul.menu-list [:li [:a {:href "/"} "Home"]]]

            [:ul.menu-list
             [:li [:a "About"]]
            ;; [:li [:a "Videos"]]
;;             [:li [:a {:href "/centers"} "Centers"]]
;;             [:li [:a {:href "/configure"} "Configure"]]
             ;;[:li [:a {:href "/questionnaire"} "Questionnaire"]]
             ]

            [:p.menu-label "Centers"]
            [:ul.menu-list
             (for [x (db.centers/centers-all db/jdbc)] [:li [:a {:href (str "/center/" (:id x))} (:name x)]
                                                        [:ul
                                                         [:li [:a {:href (str "/center/" (:id x) "/users")} "Workers"]]
                                                         [:li [:a {:href (str "/center/" (:id x) "/mothers")} "Mothers"]]]])]
                  ;; [:p.menu-label "Workers"] [:ul.menu-list [:li [:a "Dashboard"]]]]
            ]]

          [:div.column {:style "padding-left:3em;"} page]]]))
