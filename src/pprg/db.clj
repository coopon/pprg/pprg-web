(ns pprg.db
  (:require [hikari-cp.core :refer :all]
            [clojure.java.jdbc :as jdbc]
            [ragtime.jdbc :as ragtime]))

(def datasource-options (clojure.edn/read-string (slurp "resources/env/jdbc.edn")))

(defonce datasource
  (delay (make-datasource datasource-options)))

;; This object needs to be called by every HugSQL command
(def jdbc {:datasource @datasource})

(def config
  {:datastore  (ragtime/sql-database jdbc)
   :migrations (ragtime/load-resources "migrations")})
