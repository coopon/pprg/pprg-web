(ns pprg.db.users
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "pprg/db/sql/users.sql")
