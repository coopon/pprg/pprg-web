-- :name visits-insert :! :n
-- :doc Insert a single user returning affected row count
insert into visits (mother, worker, geolocation, sched_date, center, completed, number)
values (:mother, :worker, :geolocation, :sched_date, :center, false, :number)

-- :name visits-by-worker :? :*
-- :doc Get visits by worker 
select * from visits
where worker = :worker order by sched_date asc

-- :name visits-by-mother :? :*
-- :doc Get visits by mother-id
select id, mother, worker, geolocation, TO_CHAR(sched_date::DATE, 'yyyy-mm-dd') as sched_date,  TO_CHAR(visited_date::DATE, 'yyyy-mm-dd') as visited_date, center, completed, number from visits 
where mother = :mother

-- :name delete-visits-by-mother :! :1
-- :doc Delete visits by mother-id
select * from visits 
where mother = :mother  order by sched_date asc

-- :name remaining-visits-by-center :? :*
-- :doc Get incompleted visits as per center
select * from visits 
where center = :center-id and completed = false;

-- :name remaining-visits-by-center-count :? :1
-- :doc Get incompleted visits as per center
select count(*) from visits 
where center = :center-id and completed = false;

-- :name current-visit-by-mother :? :1
-- :doc Get current visit as per mother 
select * from visits 
where mother = :mother-id and completed = false
order by sched_date asc

-- :name visits-descending :? :*
-- :doc Get all visited visits descending 
select * from visits
where completed = true
order by visited_date desc

-- :name visits-descending-by-center :? :*
-- :doc Get all visited visits descending 
select * from visits
where completed = true and center = :center-id
order by visited_date desc

-- :name current-visit-by-worker :? :1
-- :doc Get current visit as per worker 
select * from visits 
where worker = :worker and completed = false
order by sched_date asc

-- :name visit-by-id :? :1
-- :doc Get visit by id 
select * from visits
where id = :id

-- :name visits-update :! :n
-- :doc Update a visit record based on id
update visits set  geolocation = :geolocation, visited_date = :visited_date,  completed = :completed
where id = :id


-- :name visits-geolocation :? :*
-- :doc Get all visited visits where geolocation isnt empty

select * from visits where completed = true and geolocation != ''
