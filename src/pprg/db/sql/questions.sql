-- :name questions-insert :! :n
-- :doc Insert a single user returning affected row count
insert into questions (worker, mother, visit, question, answer,subject,task)
values (:worker, :mother, :visit, :question, false, :subject, :task)

-- :name questions-by-visit :? :*
-- :doc Retrieve all questions associated with a visit
select * from  questions
where visit = :id

-- :name delete-questions-by-visit :! :1
-- :doc Retrieve all questions associated with a visit
delete from  questions
where visit = :id


-- :name questions-by-id :? :1
-- :doc Retrieve all questions associated with a visit
select * from  questions
where id = :id

-- :name questions-by-visit-baby-ask :? :*
-- :doc Retrieve all questions associated with a vis
select * from questions where visit = :id and subject = 'Baby' and task = 'Ask';

-- :name questions-by-visit-baby-examine :? :*
-- :doc Retrieve all questions associated with a visit
select * from questions where visit = :id and subject = 'Baby' and task = 'Examine';

-- :name questions-by-visit-baby-counsel :? :*
-- :doc Retrieve all questions associated with a visit
select * from questions where visit = :id and subject = 'Baby' and task = 'Counsel';

-- :name questions-by-visit-mother-ask :? :*
-- :doc Retrieve all questions associated with a visit
select * from questions where visit = :id and subject = 'Mother' and task = 'Ask';

-- :name questions-by-visit-mother-examine :? :*
-- :doc Retrieve all questions associated with a visit
select * from questions where visit = :id and subject = 'Mother' and task = 'Examine';

-- :name questions-by-visit-mother-counsel :? :*
-- :doc Retrieve all questions associated with a visit
select * from questions where visit = :id and subject = 'Mother' and task = 'Counsel';

-- :name questions-update :! :n
-- :doc Update a questions record based on ID
update questions set  answer = :answer
where id = :id
