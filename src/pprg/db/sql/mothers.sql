-- :name mothers-insert :<!
-- :doc Insert a single user returning affected row count
insert into mothers (name, age, blood_group, mobile, door_no, street_name, area, city, pin_code, delivery_date, child_weight, child_gender, center, worker, current_visit, deleted)
values (:name, :age, :blood_group, :mobile, :door_no, :street_name, :area, :city, :pin_code, :delivery_date, :child_weight, :child_gender, :center, :worker, 1, false) returning id

-- :name mothers-all :? :*
-- :doc Get all mothers
select name, age, blood_group, mobile, door_no, street_name, area, city, pin_code, delivery_date, child_weight, child_gender, center, worker, current_visit, deleted from mothers,visits
where deleted = false

-- :name mother-by-worker :? :*
-- :doc Get mothers by worker
select * from mothers
where worker = :worker and deleted = false

-- :name mothers-by-center :? :*
-- :doc Get mothers by center
select id, name, age, blood_group, mobile, door_no, street_name, area, city, pin_code, TO_CHAR(delivery_date::DATE, 'yyyy-mm-dd') as delivery_date, child_weight, child_gender, center, worker, current_visit, deleted from mothers
where center = :center and deleted = false

-- :name mothers-by-id :? :1
-- :doc Get a mother by id
select * from mothers
where id = :id

-- :name mothers-by-date :? :*
-- :doc Get mothers by date
select * from mothers
where :date > :date and deleted = false

-- :name mothers-count :? :1
-- :doc Get count of all mothers 
select count(*) from mothers
where deleted = false

-- :name mothers-count-by-center :? :1
-- :doc Get count of all mothers 
select count(*) from mothers
where center = :center-id and deleted = false

-- :name mothers-count-by-worker :? :1
-- :doc Get count of all mothers 
select count(*) from mothers
where worker = :email and deleted = false

-- :name mothers-delete-by-id :! :n
-- :doc Delete mother according to ID 
update mothers set deleted = true
where id = :id

-- :name mothers-update :! :n
-- :doc Update a mother record based on ID
update mothers set name = :name, age = :age, blood_group = :blood_group, mobile = :mobile, door_no = :door_no, street_name = :street_name, area = :area, city = :city, pin_code = :pin_code, delivery_date = :delivery_date, child_weight = :child_weight, child_gender = :child_gender,  worker = :worker, current_visit = :current_visit
where id = :id

-- :name mothers-update-current-visit :! :n
-- :doc Update mother's current_visit based on ID
update mothers set current_visit = :current_visit
where id = :id
