-- :name users-insert :! :n
-- :doc Insert a single user returning affected row count
insert into users (name, email, password, mobile, center, officer, deleted)
values (:name, :email, :password, :mobile, :center, :officer, false )

-- :name users-by-email :? :1
-- :doc Get user by email
select * from users
where email = :email

-- :name users-by-center :? :*
-- :doc Get user by center id
select * from users
where center = :center and deleted = false

-- :name users-all :? :*
-- :doc Get all users
select * from users
where deleted = false

-- :name users-by-id :? :1
-- :doc Get user by user ID
select * from users
where id = :id

-- :name users-count :? :1
-- :doc Get count of all users
select count(*) from users
where deleted = false

-- :name users-count-by-center :? :1
-- :doc Get count of all users in a center
select count(*) from users
where center = :center-id and deleted = false

-- :name users-update-by-id :! :n
-- :doc Update user info by user ID
update users set name = :name,  mobile = :mobile, center = :center, officer = :officer
where id = :id

-- :name users-delete-by-id-email :! :n
-- :doc Update user info by user ID & email
update users set deleted = true
where id = :id and email = :email 


