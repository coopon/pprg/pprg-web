-- :name centers-insert :! :n
-- :doc Insert a single user returning affected row count
insert into centers (name, address, geolocation, deleted)
values (:name, :address, :geolocation, false)

-- :name centers-by-id :? :1
-- :doc Get user by id
select * from centers
where id = :id

-- :name centers-all :? :*
-- :doc Get all centers
select * from centers where deleted = false;

-- :name centers-count :? :1
-- :doc Get count of all centers
select count(*) from centers where deleted = false;

-- :name center-update-by-id :! :n
-- :doc Update center info based on id
update centers set name = :name, address = :address, geolocation = :geolocation
where id = :id

-- :name center-delete-by-id :! :n
-- :doc Delete center info based on id
update centers set deleted = true
where id = :id



-- :name centers-insert-returning :!<
-- :doc Insert a single user returning affected row count
insert into centers (name, address, geolocation, deleted)
values (:name, :address, :geolocation, false) returning id
