(ns pprg.site.questions
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.site :as site]
            [pprg.site.auth :as site.auth]
            [buddy.auth :as buddy.auth]
            [pprg.db :as db]
            [pprg.db.centers :as db.centers]
            [pprg.db.users :as db.users]
            [pprg.db.mothers :as db.mothers]
            [pprg.db.questions :as db.questions]
            [pprg.db.visits :as db.visits]
;;
            ))

(defn add-questions-page [request]
  (ring-resp/response
   (site/admin-template
    "Add new question")))

(defn questions-list-page [request]
  (ring-resp/response
   (site/admin-template
    "All questions")))

(defn create-mother-visits-questions
  "Create visits for every newly created mother"
  [mother-id]
  (let [mother (db.mothers/mothers-by-id db/jdbc mother-id)
        visits (db.visits/visits-by-mother db/jdbc {:mother (:id mother)})
        data  (clojure.edn/read-string (slurp "resources/data/questionnaire.edn"))]
    (for [visit visits
          datum data]
      (if (.contains (:visits datum) (:number visit))
        (db.questions/questions-insert
         db/jdbc
         (assoc datum :worker (:worker mother) :mother (:id mother) :visit (:id visit))))
        ;;
      )))



