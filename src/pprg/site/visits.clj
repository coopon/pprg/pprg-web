(ns pprg.site.visits
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.site :as site]
            [pprg.site.auth :as site.auth]
            [buddy.auth :as buddy.auth]
            [pprg.db :as db]
            [pprg.db.centers :as db.centers]
            [pprg.db.users :as db.users]
            [pprg.db.mothers :as db.mothers]
            [pprg.db.questions :as db.questions]
            [pprg.db.visits :as db.visits])
  (:import java.time.LocalDate)
  (:import java.time.format.DateTimeFormatter))

(defn visit-add-post [request]
  (ring-resp/redirect "/"))

(defn visit-add-page [request]
  (ring-resp/response
   (site/admin-template
    "Add new visit"
    [:h1.title "Add new visit"]
    [:div.column.is-2
     [:form {:method "POST" :action "/create/visit"}
      (site/form-control "Days from childbirth" "name")
      (site/form-control-checkbox "All mothers?" "all_mothers")
      (site/form-control-button "Submit")]])))

(defn visit-page [request]
  (let
   [visit-id (Integer/parseInt (:visit-id (:path-params request)))
    visit (db.visits/visit-by-id db/jdbc {:id visit-id})
    questions (db.questions/questions-by-visit db/jdbc visit)
    mother (db.mothers/mothers-by-id db/jdbc {:id (:mother visit)})
    center (db.centers/centers-by-id db/jdbc {:id (:center mother)})
    user (db.users/users-by-email db/jdbc {:email (:worker mother)})]
    (ring-resp/response
     (site/admin-template
      (str  (:name mother) " - Visit " (:number visit))
      ;;mothers (db.mothers/mother-by-worker db/jdbc {:worker (:email user)})]
      ;;[:row
      ;; [:p.is-size-6 "Mother Name - Visit Number"]
      ;; [:p.is-size-6 "Mother Name"]
      ;; [:a {:href (str "/mother/" (:id mother))} [:h1.subtitle.is-size-2 (:name mother)]]]      ;; [:p.is-size-6 "Worker / Center"]
      ;; [:h2.subtitle.is-size-3  (:name user) " , " (:name center)] [:br]]
      [:div.columns
       [:div.column
        [:p.is-size-6 "Mother Name"]
        [:a {:href (str "/mother/" (:id mother))} [:h1.subtitle.is-size-2 (:name mother)]] [:br]
        [:p.is-size-6 "Worker / Center"]
        [:h1.subtitle.is-size-3  (:name user) " , " (:name center)]]
;;       [:p.is-size-6 "Scheduled Date"]
;;       [:h1.subtitle.is-size-3   (:sched_date visit)]

       [:div.column
        [:p.is-size-6 "Scheduled Date"]
        [:h1.subtitle.is-size-3   (:sched_date visit)]
        [:p.is-size-6 "Visited Date"]
        [:h1.subtitle.is-size-3   (if (= (:visited_date visit) nil) "Not Yet" (:visited_date visit))]]] [:hr]
      [:h1.is-size-3 "Questionnaire"] [:br]
      [:table.table.is-hoverable.is-fullwidth.is-striped [:thead [:tr [:th "Subject"]  [:th "Task"] [:th "Question"] [:th "Checklist"]]]
       [:tbody
        (for [question questions]
          [:tr   [:td (:subject question)]
           [:td (:task question)] [:td (:question question)] [:td (:answer question)]])]]))))

(defn visits-list-gtable []
  [:div.column
   [:h1.title.is-size-3 "Visits"]])

(defn create-visits-by-date [mother-id]
  (let [mother (db.mothers/mothers-by-id db/jdbc mother-id)
        days [3 7 14 22 28 42]]
    (for [day days]
      ;; (let [sched_date (.plus (LocalDate/parse (:delivery_date mother) (DateTimeFormatter/ofPattern "YYYY-MM-DD")) day java.time.temporal.ChronoUnit/DAYS)

      (let [sched_date (.plus (.toLocalDate (:delivery_date mother)) day java.time.temporal.ChronoUnit/DAYS)

            visit {:mother (:id mother-id), :worker (:worker mother), :geolocation "", :sched_date sched_date, :center (:center mother), :number (+ 1 (.indexOf days day))}]
        (db.visits/visits-insert db/jdbc visit)))))

(defn visits-leaflet-data-all []
  (for [visit (db.visits/visits-geolocation db/jdbc)]
    (str "var marker = L.marker([" (str (:geolocation visit)) "]).addTo(mymap).bindPopup(\""  (str (:name (db.mothers/mothers-by-id db/jdbc {:id (:mother visit)})) " -  "  (:visited_date visit))  "\");
")))

(defn mother-visits-table [mother-id]
  (let [mother  (db.mothers/mothers-by-id db/jdbc {:id mother-id})
        center (db.centers/centers-by-id db/jdbc {:id (:center mother)})
        visits (db.visits/visits-by-mother db/jdbc {:mother (:id mother)})]
    ;;
    [:table.table.is-hoverable.is-fullwidth.is-striped [:thead [:tr [:th "Visit No."]  [:th "Scheduled Date"] [:th "Visited Date"] [:th "Completed?"]]]
     [:tbody
      (for [visit visits]
        [:tr   [:td [:a {:href (str "/visits/" (:id visit))} (:number visit)]]  [:td [:a {:href (str "/visits/" (:id visit))} (:sched_date visit)]]
         [:td [:a {:href (str "/visits/" (:id visit))} (:visited_date visit)]] [:td [:a {:href (str "/visits/" (:id visit))} (:completed visit)]]])]]))
