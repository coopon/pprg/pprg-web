(ns pprg.site.home
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.site :as site]
            [pprg.site.auth :as site.auth]
            [buddy.auth :as buddy.auth]
            [pprg.db :as db]
            [pprg.db.centers :as db.centers]
            [pprg.db.users :as db.users]
            [pprg.db.mothers :as db.mothers]
            [pprg.db.visits :as db.visits]
            [pprg.site.centers :as site.centers]
            [pprg.site.users :as site.users]
            [pprg.site.visits :as site.visits]
            [pprg.site.mothers :as site.mothers]))

(defn home-page [request]
  ;; (if (buddy.auth/authenticated? request)
  (cond
    (zero?  (:count (db.centers/centers-count db/jdbc))) (ring-resp/redirect "/create/center")
    (zero? (:count (db.users/users-count db/jdbc))) (ring-resp/redirect "/create/user")
  ;;  (zero? (:count (db.questionnaire/questionnaire-count-all db/jdbc)))  (do (site.questionnaire/populate-db) (ring-resp/redirect "/"))
    :else
    (ring-resp/response
     (site/admin-template
      "Home"
      [:div
       [:div.columns
        [:div.column
         [:h1.is-size-3 "Home"]]
        [:div.column.has-text-right {:style "padding-right:20px;"}
         [:a.button.is-primary {:href "/create/center"} "Add new center"]]]
       [:hr]
       (site.centers/centers-list)

       [:link {:rel "stylesheet" :href "/css/leaflet.css"}]
       [:script {:src "/js/leaflet.js"}]
       [:div {:id "mapid" :style "height:300px;width:auto;"}]
       [:script "var mymap = L.map('mapid').setView([11.9373,79.7530], 13);
L.tileLayer('https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>',
    maxZoom: 18,
}).addTo(mymap);
var redIcon = L.icon({
  iconUrl: '/css/images/marker-icon-red.png',
  shadowUrl: '/css/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]});"
        (site.visits/visits-leaflet-data-all)
;;
        ]]))
;;                                             ))
  ;;   (buddy.auth/throw-unauthorized)))
        ;;(assoc :session {:sexy "cool"} 
              ;;:status 401                         
          ;;     :cookies {:bird "kek"}) 
    ))

