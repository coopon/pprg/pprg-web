(ns pprg.site.mothers
  (:require [hiccup.core :as hiccup]
            [clojure.string :as string]
            [ring.util.response :as ring-resp]
            [pprg.site :as site]
            [pprg.site.centers :as site.centers]
            [pprg.site.users :as site.users]
            [pprg.site.auth :as site.auth]
            [pprg.site.visits :as site.visits]
            [pprg.site.questions :as site.questions]
            [buddy.auth :as buddy.auth]
            [pprg.db :as db]
            [pprg.db.mothers :as db.mothers]
            [pprg.db.centers :as db.centers]
            [pprg.db.users :as db.users]
            [pprg.db.visits :as db.visits]
            [pprg.db.users :as db.users]
            [pprg.db.questions :as db.questions])
  (:import java.time.LocalDate))

(defn create-mother-html-form "Hiccup form for creating a new mother record"  [center]
  (site/admin-template
   "New mother"
   [:form {:method "POST" :action (str "/center/" (:id center)  "/mothers/create")}
    [:div.columns.is-multiline
     [:div.column.is-full
      [:h1.is-size-3 "New mother"]]
     [:div.column.is-3
      [:p.is-size-5 "General"]
      [:hr]
   ;;   (site/form-control "MCT Card No." "number")
      (site/form-control "Name" "name")
      (site/form-control-number "Age" "age" "1")
        ;; (site/form-control-date "Date of Birth" "dob")
      (site/form-control "Blood group" "blood_group")
      (site.users/users-html-dropdown (:id center) "worker")]
     [:div.column.is-3
      [:p.is-size-5 "Address"]
      [:hr]
      (site/form-control "Door No" "door_no")
      (site/form-control "Street Name" "street_name")
      (site/form-control "Area" "area")
      (site/form-control "City" "city")
      (site/form-control "Pincode" "pin_code")
      (site/form-control "Mobile" "mobile")]
     [:div.column.is-3
      [:p.is-size-5 "Baby"]
      [:hr]
      (site/form-control-date "Expected date of delivery" "delivery_date")
      (site/form-control-dropdown "Baby Sex" "child_gender" [["Male" "male"] ["Female" "female"]])
      (site/form-control-number "Baby Weight (in KG)" "child_weight" "0.1")
      (site/form-hidden-value "center" (:id center))
      (site/form-control-button "Submit")]]
  ;;                     ]
    ]))

(defn mothers-all-by-center-id [center-id]
  (let [mothers (db.mothers/mothers-by-center db/jdbc {:center center-id})]
    (for [mother mothers]
      (assoc mother
             :visits
             (let [visits (db.visits/visits-by-mother db/jdbc {:mother (:id mother)})]
               (for [visit visits]
                 (let [questions (db.questions/questions-by-visit db/jdbc  visit)]
                   (assoc
                    visit :checklist
                    {:baby {:ask (db.questions/questions-by-visit-baby-ask db/jdbc visit)
                            :examine (db.questions/questions-by-visit-baby-examine db/jdbc visit)
                            :counsel (db.questions/questions-by-visit-baby-counsel db/jdbc visit)}
                     :mother {:ask (db.questions/questions-by-visit-mother-ask db/jdbc visit)
                              :examine (db.questions/questions-by-visit-mother-examine db/jdbc visit)
                              :counsel (db.questions/questions-by-visit-mother-counsel db/jdbc visit)}}))))))))

(defn create-mother-page "HTML page for creating a new mother record" [request]
  (let [center-id (Integer/parseInt (:center-id (:path-params request)))
     ;; user (db.users/users-by-id db/jdbc {:id user-id})
        ]

    (ring-resp/response (create-mother-html-form {:id center-id}))))

(defn create-mother-post
  "Accepts new user form POST data and processes the request"
  [request]
  (let [mother-req (:form-params request)
        age  (Integer/parseInt (:age mother-req))
        center  (Integer/parseInt (:center-id (:path-params request)))
        del_date (LocalDate/parse (:delivery_date mother-req))
;;        baby_weight (string/replace (string/lower-case (:child_weight request)) "kg" "")
        ;;baby_weight (:child_weight request)
        mother (assoc mother-req :age age :delivery_date del_date :center center)
        mother-id (first (db.mothers/mothers-insert db/jdbc mother))]
    ;; Populate every new mother with necessary visit data
    (doall (site.visits/create-visits-by-date mother-id))
    (doall (site.questions/create-mother-visits-questions mother-id))
    (ring-resp/redirect (str "/center/" center "/mothers"))
   ;; (ring-resp/response (str mother-id))
    ))

(defn mothers-all "/center/:center-id/mothers/all "
  [request]
  (let [center-id (Integer/parseInt (:center-id (:path-params request)))]
    (ring-resp/response (mothers-all-by-center-id center-id))))

(defn mothers-by-worker
  "/user/:user-id/mothers/all"
  [request]
  (let [user-id (Integer/parseInt (:user-id (:path-params request)))
        user (db.users/users-by-id db/jdbc {:id user-id})
        mothers (db.mothers/mother-by-worker db/jdbc {:worker (:email user)})]
    (ring-resp/response
     (for [mother mothers]
       (assoc mother :visits
              (db.visits/visits-by-mother db/jdbc {:mother (:id mother)}))))))

(defn mother-record-page [request]
  (let
   [mother-id (Integer/parseInt (:mother-id (:path-params request)))
    mother  (db.mothers/mothers-by-id db/jdbc {:id mother-id})
    center (db.centers/centers-by-id db/jdbc {:id (:center mother)})
    visits (db.visits/visits-by-mother db/jdbc {:mother (:id mother)})
    user (db.users/users-by-email db/jdbc {:email (:worker mother)})]
    (ring-resp/response
     (site/admin-template
      (str (:name center) " - " (:name mother))
      [:div.column
       [:div.columns
        [:div.column
         [:p.is-size-6 "Mother Name"]
         [:h1.subtitle.is-size-3 (:name mother)]
         [:p.is-size-6 "Worker / Center"]
         [:h2.subtitle.is-size-3  (:name user) " , " (:name center)]
         [:p.is-size-6 "Address"]
         [:h1.subtitle.is-size-3 (str (:door_no mother) ", " (:street_name mother) ", " (:area mother) ", "  (:city mother) ", " (:pin_code mother))]
         [:p.is-size-6 "Mobile"]
         [:h1.subtitle.is-size-3 (str (:mobile mother))]
         [:p.is-size-6 "Blood Group"]
         [:h1.subtitle.is-size-3 (str (:blood_group mother))]]
        [:div.column
         [:br]
         [:h1.subtitle.is-size-3 "Child"]
         [:p.is-size-6 "Delivey Date"]
         [:h1.subtitle.is-size-3   (:delivery_date mother)]
         [:p.is-size-6 "Child Sex"]
         [:h1.subtitle.is-size-3   (:child_gender mother)]
         [:p.is-size-6 "Child Weight"]
         [:h1.subtitle.is-size-3   (:child_weight mother) " Kg"]]]
       [:hr]
       [:h1.is-size-3 "Visits Overview"]
       [:p "Click on link to view details"]
       [:br]
       (site.visits/mother-visits-table mother-id)]))))

(defn mother-record-update [request]
  (ring-resp/redirect "/"))

(defn mother-delete-page [request]
  (let [req-mother (:path-params request)
        mother-id (Integer/parseInt (:mother-id req-mother))
        mother (db.mothers/mothers-by-id db/jdbc {:id mother-id})]
    (ring-resp/response
     (site/admin-template
      "Delete mother"
      [:div
       [:h1.is-size-3 (str "Do you really want to delete " (:name mother) " ?")] [:br]
       [:form {:method "POST" :action (str "/mother/" (:id mother) "/delete")}
        [:button.button.is-danger.is-large "Delete"]]
       [:br]
       [:a.button.is-large {:href (str "/center/" (:center mother))} "No"]]))))

(defn mother-delete-post
  "POST handler for deleting center"
  [request]
  (let [mother-id (Integer/parseInt (:mother-id (:path-params request)))
        mother (db.mothers/mothers-by-id db/jdbc {:id mother-id})
        visits (db.visits/visits-by-mother db/jdbc {:mother mother-id})]
    (for [visit visits]
      (db.questions/questions-by-visit db/jdbc visit))
    (db.visits/delete-visits-by-mother db/jdbc {:mother mother-id})
    (db.mothers/mothers-delete-by-id db/jdbc {:id mother-id}))
  (ring-resp/redirect "/"))

(defn update-visit [visit]
  (if (get visit :visited_date false) visit (assoc visit :visited_date nil)))

(defn mothers-sync [request]
  (let [center-id (Integer/parseInt (:center-id (:path-params request)))
        mothers (-> request :json-params)]
    (println mothers)
    (doall
     (for [mother mothers
           :let [questions
                 (reduce
                  concat
                  (map
                   (fn [visit] (do
                                 (db.visits/visits-update
                                  db/jdbc
                                  (if (nil? (:visited_date (update-visit visit)))
                                    (update-visit visit)
                                    (update visit
                                            :visited_date
                                            (fn [date-str] (LocalDate/parse date-str)))))
                                 (concat
                                  (-> visit :checklist :baby :ask)
                                  (-> visit :checklist :baby :examine)
                                  (-> visit :checklist :baby :counsel)
                                  (-> visit :checklist :mother :ask)
                                  (-> visit :checklist :mother :examine)
                                  (-> visit :checklist :mother :counsel))))
                   (:visits mother)))]]
       (do
         (doall (map (fn [question] (db.questions/questions-update db/jdbc question)) questions))
         (db.mothers/mothers-update-current-visit db/jdbc mother))))

    (ring-resp/response (mothers-all-by-center-id center-id))))
