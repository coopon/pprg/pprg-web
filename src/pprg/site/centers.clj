(ns pprg.site.centers
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.site :as site]
            [pprg.site.auth :as site.auth]
            [pprg.site.visits :as site.visits]
            [buddy.auth :as buddy.auth]
            [pprg.db :as db]
            [pprg.db.centers :as db.centers]
            [pprg.db.users :as db.users]
            [pprg.db.mothers :as db.mothers]
            [pprg.db.visits :as db.visits]
            [hiccup.page :as page]))

(defn centers-html-dropdown []
  (let [centers (pprg.db.centers/centers-all pprg.db/jdbc)]
    (site/form-control-dropdown "Center" "center"
                                (map #(vector (:name %) (:id %))  centers))))

(defn create-center-html-form []
  (site/admin-template
   "Create new center"
   [:div.column.is-one-fifth.is-offset-one-third
    [:h1.title.has-text-centered "New Center"]
    [:form {:method "POST" :action "/create/center"}
     (site/form-control "Center Name" "name")
     (site/form-control "Address" "address")
     (site/form-control "Geolocation" "geolocation")
     (site/form-control-button "Submit")]]))

(defn create-center-page [request]
  (ring-resp/response (create-center-html-form)))

(defn create-center-post
  "Handle POST url-encoded request and push it to DB" [request]
  (let [center (:form-params request)]
    (db.centers/centers-insert db/jdbc center)
    (ring-resp/redirect "/")))

(defn edit-center-page [request]
  (ring-resp/response
   (site/admin-template
    "Edit Center"
    (let [center-id (Integer/parseInt (:center-id (:path-params request)))
          center (db.centers/centers-by-id db/jdbc {:id center-id})]
      [:div.column.is-one-fifth.is-offset-one-third
       [:h1.title.has-text-centered "Edit Center"]
       [:form {:method "POST" :action (str "/center/" center-id "/edit")}
        (site/form-hidden-value "Center name" "id" center-id)
        (site/form-control "Center Name" "name" (:name center))
        (site/form-control "Address" "address" (:address center))
        (site/form-control "Geolocation" "geolocation" (:geolocation center))
        (site/form-control-button "Submit")]]))))

(defn edit-center-post
  "Handle POST url-encoded request and push it to DB" [request]
  (let [req-center (:form-params request)
        center-id (Integer/parseInt (:id req-center))
        center (assoc req-center :id center-id)]
    (db.centers/center-update-by-id db/jdbc center)
    (ring-resp/redirect "/")))

(defn centers-list []
  (let [centers (db.centers/centers-all db/jdbc)]
    "All Centers"
    [:table.table.is-hoverable.is-fullwidth.is-striped [:thead [:tr [:th "Center Name"]  [:th "Workers"] [:th "Mothers"] [:th "Remaining Visits"]  [:th "Edit"] [:th "Delete"]]]
     [:tbody
      (for [center centers]
        [:tr [:td  (:name center)]  [:td (:count (db.users/users-count-by-center db/jdbc {:center-id (:id center)}))]
         [:td (:count (db.mothers/mothers-count-by-center db/jdbc {:center-id (:id center)}))]
         [:td  (:count (db.visits/remaining-visits-by-center-count db/jdbc {:center-id (:id center)}))]
         [:td [:a {:href (str "/center/" (:id center)  "/edit")} [:i.fa.fa-pencil {:aria-hidden "true"}]]]
         [:td [:a {:href (str "/center/" (:id center)  "/delete")} [:i.fa.fa-trash {:aria-hidden "true"}]]]])]]))

(defn center-delete-page [request]
  (let [req-center (:path-params request)
        center-id (Integer/parseInt (:center-id req-center))
        center (db.centers/centers-by-id db/jdbc {:id center-id})]
    (ring-resp/response
     (site/admin-template
      "Delete center"
      [:div
       [:h1.is-size-3 (str "Do you really want to delete " (:name center) " ?")] [:br]
       [:form {:method "POST" :action (str "/center/" (:id center) "/delete")}
        [:button.button.is-danger.is-large "Delete"]]
       [:br]
       [:a.button.is-large {:href (str "/center/" (:id center))} "No"]]))))

(defn center-delete-post
  "POST handler for deleting center"
  [request]
  (let [center-id (Integer/parseInt (:center-id (:path-params request)))]
    (db.centers/center-delete-by-id db/jdbc {:id center-id}))
  (ring-resp/redirect "/"))

(defn center-overview-page [request]
  (let [center-id (Integer/parseInt (:center-id (:path-params request)))
        center (db.centers/centers-by-id db/jdbc {:id center-id})
        users (db.users/users-by-center db/jdbc {:center center-id})
        mothers (db.mothers/mothers-by-center db/jdbc {:center center-id})
;;        past-visits (db.visits/visits-descending db/jdbc)
;;
        ]
    (ring-resp/response
     (site/admin-template
      (:name center)
      [:script {:src "/js/leaflet.js"}]
      [:link {:rel "stylesheet" :href "/css/leaflet.css"}]
      [:h1.is-size-3 (:name center)]
      [:hr]
      [:div
       [:h1.is-size-4 "Upcoming visits"]
       [:br]
       [:table.table.is-hoverable.is-fullwidth.is-striped
        [:thead [:tr [:th "Date"] [:th "Visit No."] [:th "Mother"] [:th "Worker"]]]
        [:tbody
         (for [mother mothers]
           (let [visit (db.visits/current-visit-by-mother db/jdbc {:mother-id (:id mother)})
                 worker (db.users/users-by-email db/jdbc {:email (:worker mother)})]
             [:tr [:td (:sched_date visit)]  [:td (:number visit)] [:td (:name mother)]
              [:td (:name worker)]]))]]
       [:hr]
       [:h2.is-size-5 "Past visits"]
       [:br]
       [:table.table.is-hoverable.is-fullwidth.is-striped
        [:thead [:tr [:th "Scheduled Date"] [:th "Visited Date"] [:th "Visit No."] [:th "Mother"] [:th "Worker"]]]
        [:tbody
         (for [visit (db.visits/visits-descending-by-center db/jdbc {:center-id center-id})]
           (let [mother (db.mothers/mothers-by-id db/jdbc {:id (:mother visit)})
                 worker (db.users/users-by-email db/jdbc {:email (:worker mother)})]
             [:tr [:td (:sched_date visit)] [:td (:visited_date visit)]  [:td (:number visit)] [:td (:name mother)]
              [:td (:name worker)]]))]]
       ;;[:h2.is-size-5 (:name user)] [:a.button {:href (str "/user/" (:id user) "/mothers/create")} "Add mother"]
       ]))))

(defn users-all-page [request]
  (let
   [center-id (Integer/parseInt (:center-id (:path-params request)))
    center (db.centers/centers-by-id db/jdbc {:id center-id})
    users (db.users/users-by-center db/jdbc {:center center-id})]
    (ring-resp/response
     (site/admin-template
      (:name center)
      [:div.columns
       [:div.column
        [:h1.is-size-3 (str (:name center) " Workers")]]
       [:div.column.has-text-right {:style "padding-right:20px;"}
        [:a.button.is-primary {:href "/create/user"} "Add new worker"]]]
      [:hr]
      [:table.table.is-hoverable.is-fullwidth.is-striped [:thead [:tr [:th "Worker Name"]  [:th "Next Visit"] [:th "Mothers"] [:th "Edit"] [:th "Delete"]]]
       [:tbody
        (for [user users]
          (let [visit (db.visits/current-visit-by-worker db/jdbc {:worker (:email user)})]
            [:tr [:td (:name user)]  [:td (:sched_date visit)] [:td (:count (db.mothers/mothers-count-by-worker db/jdbc user))]
             [:td [:a {:href (str "/user/" (:id user)  "/edit")} [:i.fa.fa-pencil {:aria-hidden "true"}]]]
             [:td [:a {:href (str "/user/" (:id user)  "/delete")} [:i.fa.fa-trash {:aria-hidden "true"}]]]]))]]
                             ;; [:div.column.is-half [:a.button {:href (str "/user/" (:id user) "/mothers/create")} "Add mother"]]]
      ))))

(defn mothers-all-page [request]
  (let
   [center-id (Integer/parseInt (:center-id (:path-params request)))
    center (db.centers/centers-by-id db/jdbc {:id center-id})
    mothers (db.mothers/mothers-by-center db/jdbc {:center center-id})]
    (ring-resp/response
     (site/admin-template
      (:name center)
      [:div.columns
       [:div.column
        [:h1.is-size-3 (str (:name center) " Mothers")]]
       [:div.column.has-text-right {:style "padding-right:20px;"}
        [:a.button.is-primary {:href (str "/center/" center-id "/mothers/create")} "Add new mother"]]]
      [:hr]
      [:table.table.is-hoverable.is-fullwidth.is-striped [:thead [:tr [:th "Mother Name"] [:th "Next Visit"] [:th "Age"] [:th "Worker"] [:th "Baby DOB"] [:th "Baby Sex"] [:th "Edit"] [:th "Delete"]]]
       [:tbody
        (for [mother mothers]
          (let [visit (db.visits/current-visit-by-mother db/jdbc {:mother-id (:id mother)})]
            [:tr [:td [:a {:href (str "/mother/" (:id mother))} (:name mother)]]  [:td (:sched_date visit)] [:td (:age mother)] [:td (:worker mother)] [:td (:delivery_date mother)] [:td (:child_gender mother)]
             [:td [:a {:href (str "/mother/" (:id mother) "/edit")} [:i.fa.fa-pencil {:aria-hidden "true"}]]]
             [:td [:a {:href (str "/mother/" (:id mother)  "/delete")} [:i.fa.fa-trash {:aria-hidden "true"}]]]]))]]
             ;;[:div.column.is-half [:a.button {:href (str "/user/" (:id user) "/mothers/create")} "Add mother"]]]
      ))))

