(ns pprg.site.auth
  (:require [buddy.hashers :as hashers]
            [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.site :as site]
            [pprg.db.users :as users]
            [pprg.db :as db]))

(defn user-by-email-exists?
  "Checks if user for the given email exists"
  [email]
  (let [data (users/users-by-email db/jdbc email)]
    (nil? data)))
;;https://juxt.pro/blog/posts/securing-your-clojurescript-app.html

(defn authenticate
  "Authenticates the user. First checks if the user exists"
  [data]
  (let [user (users/users-by-email db/jdbc data)]
    (if (nil? user)
      {:err_msg "Username or Password is wrong"}
      (let [attempt (:password data)
            password (:password user)]
        (if (hashers/check attempt password)
          user
          {:err_msg "Username or Password is wrong"})))))

(defn derive-password [data]
  (hashers/derive  data {:alg :bcrypt+blake2b-512}))

(defn login-post [request]
  (-> (ring-resp/redirect "/")
      (assoc :session {:token "token"})))

(defn register-post [request]
  (-> (ring-resp/redirect "/")
      (assoc :session {:token "token"})))

(defn auth-control []
  [:div.column.is-one-third.is-offset-one-third
   [:h1.title.has-text-centered "Create a new user"]
   [:form {:method "POST" :action "/users/login"}
    (site/form-control-email "Email" "email")
    (site/form-control-password "Password" "password")
    (site/form-control-button "Submit")]])

(defn auth-page [request]
  (ring-resp/response (site/base
                       "Login"
                       (auth-control))))
