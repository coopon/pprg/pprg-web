(ns pprg.site.users
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.site :as site]
            [pprg.site.centers :as site.centers]
            [pprg.site.auth :as site.auth]
            [buddy.auth :as buddy.auth]
            [buddy.hashers :as buddy.hashers]
            [pprg.db.users :as db.users]
            [pprg.db.centers :as db.centers]
            [pprg.db :as db]))

(defn create-user-html-form []
  [:div.column.is-one-fifth.is-offset-one-third
   [:h1.title.has-text-centered "New User"]
   [:form {:method "POST" :action "/create/user"}
    (site/form-control "Name" "name")
    (site/form-control-email  "Email" "email")
    (site/form-control-password "Password" "password")
    (site/form-control "Mobile" "mobile")
    ;; (for [center (pprg.db.centers/centers-all pprg.db/jdbc)]
    (site.centers/centers-html-dropdown)
    (site/form-control-checkbox "Admin?" "officer")
    (site/form-control-button "Submit")]])

(defn create-user-page [request]
  (ring-resp/response
   (site/admin-template
    "Create a new user"
    (create-user-html-form))))

(defn user-all [request]
  (ring-resp/response
   (db.users/users-all db/jdbc)))

(defn user-record-api
  "Return a single user record in JSON format"
  [request]
  (let [req-user (:path-params request)
        user-id (Integer/parseInt (:user-id req-user)) ;;Convet string to Intger        
        user (db.users/users-by-id db/jdbc {:id user-id})]
    (ring-resp/response user)))

(defn create-user-post
  "Accepts new user form POST data and processes the request"
  [request]
 ;; (ring-resp/response request))
  (let [user (:form-params request)
        password (:password user)]
    (db.users/users-insert db/jdbc
                           (->
                            (assoc user :officer (if (= (:officer user) "on") true false))
                            (assoc :center (Integer/parseInt (:center user)))
                            (assoc :password (site.auth/derive-password password))))
    (ring-resp/redirect "/")))

(defn api-login-post
  ""
  [request]
  (let [user (:json-params request)
        result (site.auth/authenticate user)
        center (db.centers/centers-by-id db/jdbc {:id (:center result)})
        user-full (assoc result :center-name (:name center))]
    (ring-resp/response  user-full)))
     ;; (ring-resp/response {:err_msg "Username or Password is wrong"}))))

(defn login-post ""
  [request]
  (ring-resp/redirect "/"))

(defn user-edit-page [request]
  (ring-resp/response
   (site/admin-template "User"
                        (let
                         [user-id {:id (Integer/parseInt (:user-id (:path-params request)))}
                          user (db.users/users-by-id db/jdbc user-id)]
                          [:div.column.is-one-quarter.is-offset-one-quarter
                           [:h1.title.has-text-centered "Edit User"]
                           [:form {:method "POST" :action (str "/user/" (:id user-id) "/edit")}
                            (site/form-hidden-value "User ID" "id" (:id user-id))
                            (site/form-control "Name" "name" (:name user))
                            (site/form-control "Mobile" "mobile" (:mobile user))
                            (site.centers/centers-html-dropdown)
                            (site/form-control-checkbox "Admin?" "officer" "checked")

                            (site/form-control-button "Submit")]]))))

(defn user-edit-post [request]
  (let [req-user (:form-params request)
        user-id (Integer/parseInt (:id req-user)) ;;Convet string to Intger        
        user (assoc req-user :id user-id)] ;; Assoc the new Int value instead of original string 
    (db.users/users-update-by-id db/jdbc
                                 (->
                                  (assoc user :officer (if (= (:officer user) "checked") true false))
                                  (assoc :center (Integer/parseInt (:center user)))))
    (ring-resp/redirect "/")))

(defn user-delete-page [request]
  (let [req-user (:path-params request)
        user-id (Integer/parseInt (:user-id req-user))
        user (db.users/users-by-id db/jdbc {:id user-id})]
    (ring-resp/response
     (site/admin-template
      "Delete user"
      [:div.column.is-one-third.is-offset-one-fifth
       [:h1.is-size-3 (str "Do you really want to delete " (:name user) " ?")] [:br]
       [:form {:method "POST" :action (str "/user/" (:id user) "/delete")}
        [:p.is-size-5 "Please enter user's email to confirm"]
        (site/form-control-email "Email" "email")
        [:button.button.is-danger.is-large "Delete"]]
       [:br]
       [:a.button.is-large {:href (str "/center/" (:center user))} "No"]]))))

(defn user-delete-post [request]
  (let [user-id (Integer/parseInt (:user-id (:path-params request)))
        email (:email (:form-params request))]
    (prn {:id user-id :email email})
    (db.users/users-delete-by-id-email db/jdbc {:id user-id :email email}))
  (ring-resp/redirect "/"))

(defn users-html-dropdown [center-id param-name]
  (let [users (db.users/users-by-center pprg.db/jdbc {:center center-id})]
    (site/form-control-dropdown "User" param-name
                                (map #(vector (:name %) (:email %))  users))))

