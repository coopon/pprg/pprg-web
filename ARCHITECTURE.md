# System architecture

## Data Models

### centers

| Column | Datatype |
| --- | --- | 
| id | SERIAL PRIMARY KEY |
| name | VARCHAR(255) |
| address |  VARCHAR(255) |
| geolocation | VARCHAR(100) |

### User 

| Column | Datatype | 
| --- | --- | 
| id |  SERIAL | 
| email | VARCHAR(100) PRIMARY KEY |
| password | VARCHAR(255) |
| mobile | VARCHAR(12) |
| created_at | TIMESTAMP |
| deleted_at |  TIMESTAMP |
| deleted | BOOLEAN |
| center | INT REFERENCES centers(id) |
| officer | BOOLEAN |


### mothers

| Column | Datatype |
| --- | --- | 
| id |  SERIAL PRIMARY KEY |
| type VARCHAR(10),
| name VARCHAR(255),
| age INT,
| blood_group VARCHAR(10),
| mobile VARCHAR(12),
| street_name VARCHAR(150),
| area VARCHAR(150),
| city VARCHAR(150),
| pin_code VARCHAR(10),
| delivery_date DATE,
| child_weight VARCHAR(10),
| child_gender VARCHAR(10),
| center INT references centers(id),
| worker VARCHAR(255) references users(email),
| current_visit |  INT |

## REST API

### CENTER 

Use web form

#### POST - URL-ENCODED - /create/center 
| params      | value              | type   |
| ---         | ---                | ---    |
| name        | "FSHM"             | string |
| address     | "Laporte Street"   | string |
| geolocation | "12.2111,79.13123" | string |

On Success - Redirect "/"
On Failure - Redirect "/create/center"

### USER

#### GET - /users 

#### POST - URL-ENCODED - /create/user
Recommended - only use web form

| params   | value             | type              |
| ---      | ---               | ---               |
| email    | "email@email.com" | string            |
| password | "password"        | string            |
| center   | 2                 | int (foreign key) |
| mobile   | "9876543210"      | string            |
| admin    | "on" or "off"     | string            |



#### POST - JSON - /users/authenticate
| params     | value             | type   |
| ---        | ---               | ---    |
| "email"    | "email@email.com" | string |
| "password" | "password"        | string |

On success - Full user profile data
On failure - JSON - {"err_msg" : "Username or Password is wrong"}

#### POST - URL-ENCODED - /user/<user-id>/update

### MOTHER

#### POST - URL-ENCODED - /user/<user-id>/mothers/create

### MEETING 

### 

### 
