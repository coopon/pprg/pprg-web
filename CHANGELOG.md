# pprg CHANGELOG

## [0.8.4] - Unreleased
#### Added 
- pprg.site/admin-template - Add home page link
- 

#### 


## [0.8.3] - 2019-07-27
### Changed
- site.mothers/mothers-all is now site.mothers/mothers-by-worker

### Fixed
- site.auth/authenticate - Returns error message if user not found
- site.mothers/mothers-by-worker - Correctly output only user related mothers

## [0.8.2] - 2019-07-25
### Added 
- site.mothers/mothers-all - Added route /user/<user-id>/mothers/all to drop all mother data relating to a user

## [0.8.1] - 2019-07-24
### Fixed 
- site.users/api-login-post - Returns error message on incorrect authentication

### Changed 
- project.clj deps - Update [hikari-cp] from "2.7.1" to "2.8.0"
- project.clj deps - Update [buddy/buddy-auth] from "2.1.0" to "2.2.0"
- project.clj deps - Update [buddy/buddy-hashers] from "1.3.0" to "1.4.0"
- site.auth/authenticate - Returns an err_msg hash-map instead of false for incorrect authentication

## [0.8.0] - 2019-07-23
### Added 
- ARCHITECTURE.md - Add REST API details, update existing documentation

### Changed
- migrations/003-create-mothers.edn - Changed the datatype of delivery_date from TIMESTAMP to DATE
- sql/mothers.sql - Specified columns for the INSERT statement 
- site/form-control-button - Increase button size to medium
- site/admin-template - Add right border to menu, add necessary padding
- centers/center-overview-page - Now lists mothers according to the workers they are attached with
- mothers/create-mother-html-form - Split the form into 3 columns, reorder elements
- mothers/create-mother-post - Type cast required params and push into db

## [0.7.0] - 2019-07-16

### Added 
- pprg.site/admin-template - Template for admin dashboard, contains a menu
- 'mothers' table - Add VARCHAR door_no & street_name
- db.centers - Add centers-count SQL query to count all centers
- db.mothers - Add mothers-count SQL query to count all mothers
- db.users - Add users-count SQL query to count all users
- db.users - Add users-by-id SQL to get one unique user by their email id
- pprg.site - Add form-control-textarea, form-control-date, form-hidden-value
- pprg.site - Start work on a dash counter card
- pprg.centers - center-overview-page - Gives brief overview of the center
- Add routes and (only) functions for individual users,mothers and their updates

### Changed
- 'mothers' table - 'type' has a DEFAULT constraint 'mother' 
- pprg.site - Update Bulma from [0.7.4] to [0.7.5]
- ARCHITECTURE.md - Tidy up old definitions 
- site.home/home-page - Redirects to user or center creation page if nothing is found
- pprg.service/routes - Reorder, refactor routes for accurate name resolution
- site.mothers/create-mother-html-form - Update fields to match JSON interchange data structure

## [0.6.1] - 2019-07-03
### Fixed
- pprg.site.users/authenticate - Forced evaluation of lazy functions

## [0.6.0] - 2019-07-01
### Added 
- pprg.site.auth/derive-password - A commmon password hashing function containing the default settings for the application 
- pprg.site.auth/authenticate - Authenticates a email and password, also checks if user exists
- pprg.site.auth/auth-control - Hiccup control for user login
- pprg.site.users - New users passwords are now hased before storing
- pprg.service - Add routes for /users/login {GET, POST} /users/authenticate - POST

### Changed 
- CHANGELOG.md - Correct typo
- pprg.service/service - Change port from 8080 to 6666
- project.clj deps - Update [org.clojure/clojure] from "1.10.0" to "1.10.1"
- project.clj deps - Update [io.pedestal/pedestal.service] from "0.5.5" to "0.5.7"
- project.clj deps - Update [io.pedestal/pedestal.jetty] from "0.5.5" to "0.5.7"
- project.clj deps - Update [org.postgresql/postgresql] from "42.2.5" to "42.2.6"
- project.clj deps - Update [io.pedestal/pedestal.service-tools] (dev only) from "0.5.5" to "0.5.7"
- users.sql - Change typos to fetch all records for select statements
- mothers.sql - Change typos to fetch all records for select statements

## [0.5.0] - 2019-06-21
### Added 
- pprg.db.centers/centers-all-names - A new SQL query to fetch just the names for the centers 
- pprg.db.users/users-insert Added deleted column and default value of false
- /users/create - User creation via web form fully works 
- project.clj - Add git repo URL for :url param

### Changed
- pprg.db.centers/centers-all - Corrected documentation 
- pprg.db.users/users-insert - Corrected syntax error by adding specific column names, redirects to homepage for now
- pprg.site/form-control-dropdown - Accept a collection of value pairs for each option and form value
- pprg.site.users - Rename all worker named functions to user for consistency
- pprg.service - Rebind /users/new POST into /users/create POST


## [0.4.0] - 2019-06-20
### Added 
- pprg.site.home - Added checks for availability of centers / users / mothers and to respond with respective forms in the homepage if there are none
- Routes for /centers/create, /centers/post, /mothers/create, /mothers/post
- Route names for /centers/create to differentiate between get & post
- pprg.site.centers/create-center-post now handles url-encoded POST data from /centers/create  and pushes it to DB

### Changed
- Removed lowercasing for arguments in pprg.site dynamic hiccup forms
- Correct behaviour for pprg.site/form-control-dropdown
- Add missing parantheses to pprg.db.centers/centers-insert SQL statement 

## [0.3.0] - 2019-06-18
### Added 
- [cheshire "5.8.1."] as dependency
- name VARCHAR(255) to users table
- users-all query to users.sql to fetch all users
- JSON encoding middleware as seen on pedestal-samples
- Routes for /users, /users/create, /users/post 
- 'name' HTML field for all hiccup dynamic controls
- Checks and dynamic HTML generation for centers, mothers and users on the homepage

## [0.2.0] - 2019-06-10
### Added
- Started on center specific status overview for the homepage
- Created table migrations for questions
- Wrote basic SQL queries for visits, mothers and questions tables
- CASCADE to all migration rollbacks

### Changed
- id on the visits table is now a primary key
- ARCHITECTURE.md now reflects the current status of the tables with datatypes matching PostgreSQL


## [0.1.0] - 2019-06-09
Started project using column template
### Added 
- Data models for centers, users, mothers, visits
- SQL queries for centers
- Hiccup templates for centers, users, mothers
