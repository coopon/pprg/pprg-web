(defproject pprg "0.8.5"
  :description "pprg"
  :url "https://gitlab.com/coopon/pprg/pprg-web/"
  :license {:name "AGPLv3 with Classpath exception"
            :url "https://www.gnu.org/licenses/agpl.txt"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [io.pedestal/pedestal.service "0.5.7"]
                 [io.pedestal/pedestal.service-tools "0.5.7"]
                 ;; Remove this line and uncomment one of the next lines to
                 ;; use Immutant or Tomcat instead of Jetty:
                 ;; [io.pedestal/pedestal.jetty "0.5.7"]
                 [io.pedestal/pedestal.immutant "0.5.5"]
                 ;; [io.pedestal/pedestal.tomcat "0.5.5"]

                 [ch.qos.logback/logback-classic "1.2.3" :exclusions                  [org.slf4j/slf4j-api]]
                 [org.slf4j/jul-to-slf4j "1.7.29"]
                 [org.slf4j/jcl-over-slf4j "1.7.29"]
                 [org.slf4j/log4j-over-slf4j "1.7.29"]
                 [hiccup "2.0.0-alpha2"]
                 ;;[org.clojure/clojurescript "1.10.520"]
                 [hikari-cp "2.9.0"]
                 [mount "0.1.16"]
                 [ragtime "0.8.0"]
                 [org.postgresql/postgresql "42.2.8"]
                 [com.layerware/hugsql "0.5.1"]
                 [buddy/buddy-auth "2.2.0"]
                 [buddy/buddy-hashers "1.4.0"]
                 [cheshire "5.9.0"]]

  :min-lein-version "2.0.0"
  :resource-paths ["config", "resources"]
  ;; If you use HTTP/2 or ALPN, use the java-agent to pull in the correct alpn-boot dependency
  ;:java-agents [[org.mortbay.jetty.alpn/jetty-alpn-agent "2.0.5"]]
  :profiles {:dev {:aliases {"run-dev" ["trampoline" "run" "-m" "pprg.server/run-dev"]
                             "migrate"  ["run" "-m" "pprg.server/migrate"]
                             "rollback" ["run" "-m" "pprg.server/rollback"]}
                   :dependencies [[io.pedestal/pedestal.service-tools "0.5.7"]]}
             :uberjar {:aot [pprg.server]}}
  :main ^{:skip-aot true} pprg.server)

